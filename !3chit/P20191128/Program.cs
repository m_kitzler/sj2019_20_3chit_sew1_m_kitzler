﻿using System;
using System.Text.RegularExpressions;

namespace P20191128
{
    class Program
    {
        static void Main(string[] args)
        {
            Regex rgx = new Regex(@"0(664|672|666)\s(\d{6}|\d{7})");
            Console.WriteLine(rgx.IsMatch("0664 123456"));
            Console.WriteLine(rgx.IsMatch("0664 1234567"));
            Console.WriteLine(rgx.IsMatch("0664 23456"));
            Console.WriteLine(rgx.IsMatch("0672 123456"));
            Console.WriteLine(rgx.IsMatch("0666 1234567"));
        }
    }
}
