﻿using System;

namespace P20191205
{
    class Person : IComparable
    {
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public Person(string Vorname, string Nachname)
        {
            this.Vorname = Vorname;
            this.Nachname = Nachname;
        }
        public override string ToString()
        {
            return Vorname + " " + Nachname;
        }

        public int CompareTo(object obj)
        {
            int hlp = Nachname.CompareTo((obj as Person).Nachname);
            return (hlp == 0) ? Vorname.CompareTo((obj as Person).Vorname) : hlp;
        }
    }
}
