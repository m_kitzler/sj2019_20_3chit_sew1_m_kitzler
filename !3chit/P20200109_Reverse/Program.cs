﻿using at.htlkrems;
using System;
using System.Text;

namespace P20200109_Reverse
{
    public static class Extensions
    {
        public static string Multiply(this string s, int count)
        {
            StringBuilder sb = new StringBuilder(s);
            for (int i = 0; i < count; i++)
                sb.Append(s);
            return sb.ToString();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!".Reverse());
            Console.WriteLine("Otto".IsPalindromWord());
            Console.WriteLine("Peter".IsPalindromWord());
            DateTime start = DateTime.Now;
            for (int i = 0; i < 1000000; i++)
            {
                "Alle Bananen, Anabella!".IsPalindromSentence(',', ' ', '.', ';', '!');
            }
            Console.WriteLine(DateTime.Now - start);
        }
    }
}
