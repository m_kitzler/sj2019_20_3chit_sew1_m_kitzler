﻿using at.htlkrems._3chit.games;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace P20200423_TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string player = "X";
        TTT t3 = new TTT();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            t3.Setze(Convert.ToInt32((sender as TextBlock).Tag), player);
            Resources["field" + (sender as TextBlock).Tag] = player;
            player = (player == "O") ? "X" : "O";
            (sender as TextBlock).IsEnabled = false;
            string winner = t3.Gewinnermittlung();
            if (winner != null)
            {
                MessageBox.Show((winner == "draw" ? "Keiner" : winner) + " hat Gewonnen");
                for (int i = 1; i <= 9; i++)
                {
                    Resources["field" + i] = null;
                    foreach (TextBlock tb in new TextBlock[] { tb1, tb2, tb3, tb4, tb5, tb6, tb7, tb8, tb9 })
                    {
                        tb.IsEnabled = true;
                    }
                }
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Resources["fontSize"] = tb1.ActualHeight / 1.4;
        }
    }
}
