﻿using System;
using System.Collections;

namespace P20191107_Lib
{
    public class DynList<T> : IEnumerable where T : IComparable
    {
        public Element<T> root { get; private set; }

        public DynList(T root)
        {
            this.root = new Element<T>(root);
        }

        public T this[int i]
        {
            get
            {
                Element<T> hlp = root;
                while (i >= 0)
                {
                    if (i == 0)
                        return hlp.Value;
                    hlp = hlp.NextElement;
                    i--;
                }
                throw new IndexOutOfRangeException();
            }
            set
            {
                Element<T> hlp = root;
                while (i >= 0)
                {
                    if (i == 0)
                        hlp.Value = value;
                    else
                    {
                        hlp = hlp.NextElement;
                        i--;
                    }
                }
                if (i < 0)
                    throw new IndexOutOfRangeException();
            }
        }

        public Element<T> GetElement(int i)
        {
            Element<T> hlp = root;
            while (i >= 0)
            {
                if (i == 0)
                    return hlp;
                hlp = hlp.NextElement;
                i--;
            }
            throw new IndexOutOfRangeException();
        }

        public void AddAtEnd(T toAdd)
        {
            Element<T> hlp = root;
            while (hlp.NextElement != null)
            {
                hlp = hlp.NextElement;
            }
            hlp.NextElement = new Element<T>(toAdd);
        }

        public void AddAtBegin(T toAdd)
        {
            Element<T> hlp = root;
            root = new Element<T>(toAdd);
            root.NextElement = hlp;
        }

        public int Length
        {
            get
            {
                int i = 1;
                Element<T> hlp = root;
                while (hlp.NextElement != null)
                {
                    i++;
                    hlp = hlp.NextElement;
                }
                return i;
            }
        }

        private int Count(Element<T> current)
        {
            if (current == null) return 0;
            else return Count(current.NextElement) + 1;
        }

        public int Count() { return Count(root); }

        //Alternativ (und besser ;])
        public int Anzahl() => Anzahl(root);

        private int Anzahl(Element<T> current) => current == null ? 0 : Anzahl(current.NextElement) + 1;

        public string RevPrint() => RevPrint(root);

        private string RevPrint(Element<T> current) => (current.NextElement == null) ? current.Value.ToString() : RevPrint(current.NextElement) + ", " + current.Value;

        public string Print()
        {
            string retVal = root.Value.ToString();
            Element<T> hlp = root;
            while (hlp.NextElement != null)
            {
                hlp = hlp.NextElement;
                retVal = retVal + ", " + hlp.Value;
            }
            return retVal;
        }

        public IEnumerator GetEnumerator()
        {
            return new DynListIterator<T>(this);
        }
    }
}
