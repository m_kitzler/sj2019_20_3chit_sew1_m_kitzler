﻿using System.Collections.Generic;

namespace P20200514_AlgoTestSpielesammlung
{
    class StringComp
    {
        List<string> strings = new List<string>();
        public List<string> errors { get; private set; } = new List<string>();

        public void AddString(string add)
        {
            strings.Add(add);
        }

        public void Compare()
        {
            for (int i = 0; i < strings[0].Length; i++)
            {
                if (strings[0][i] != strings[1][i] || strings[0][i] != strings[2][i] || strings[0][i] != strings[3][i])
                {
                    errors.Add(i + ": " + strings[0][i] + " " + strings[1][i] + " " + strings[2][i] + " " + strings[3][i]);
                }
            }
        }
    }
}
