﻿using at.htlkrems._3chit.games;
using System;
using System.Linq;

namespace P20200514_AlgoTestSpielesammlung
{
    class Program
    {
        static void Main(string[] args)
        {
            int xSize = 7, ySize = 70000000;
            VierGewinnt vg = new VierGewinnt(xSize, ySize);
            #region Feld erstellen
            Console.WriteLine("Creating Field\n");
            Random r = new Random();
            for (int x = 0; x < xSize; x++)
            {
                int prog = 0;
                int part = ySize / 10;
                int currPos = 0;
                do
                {
                    int ran = r.Next(0, 3);
                    currPos = vg.Setze(x, ran == 0 ? null : ran == 1 ? "SP1" : "SP2").Item1;
                    int currProg = 10 - currPos / part;
                    if (currProg > prog)
                    {
                        prog = currProg;
                        Console.CursorTop--;
                        Console.WriteLine("{0}. Spalte: [{1}{2}]", x + 1, String.Concat(Enumerable.Repeat("O", prog)), String.Concat(Enumerable.Repeat(" ", 10 - prog)));
                    }
                }
                while (currPos > 0);
                Console.WriteLine();
            }
            #endregion
            #region GUI
            Console.WriteLine("Field Size: {0}x{1}", xSize, ySize);
            Console.Write("Algo"); Console.CursorLeft = 12;
            Console.Write("Zeit"); Console.CursorLeft = 33;
            Console.Write("Gewonnen: null"); Console.CursorLeft = 53;
            Console.Write("SP1"); Console.CursorLeft = 63;
            Console.Write("SP2");
            Console.WriteLine("\n");
            #endregion

            //StringComp sc = new StringComp(); //Debug
            Func<int, string>[] algo = new Func<int, string>[] { vg.GEHorizontal, vg.GEHLug, vg.GEHPoem, vg.GEHPoemMod };
            for (int i = 0; i < algo.Length; i++)
            {
                //StringBuilder sb = new StringBuilder(); //Debug
                Console.Write(i == 0 ? "if" : i == 1 ? "Lugmayr" : i == 2 ? "Poemmer" : "Poem Mod");
                int countN = 0;
                int count1 = 0;
                int count2 = 0;
                DateTime start = DateTime.Now;
                for (int x = 0; x < ySize; x++)
                {
                    switch (algo[i](x))
                    {
                        case null:
                            countN++;
                            //sb.Append("0");   //Debug
                            break;
                        case "SP1":
                            count1++;
                            //sb.Append("1");   //Debug
                            break;
                        case "SP2":
                            count2++;
                            //sb.Append("2");   //Debug
                            break;
                    }
                }
                DateTime stop = DateTime.Now;
                Console.CursorLeft = 12;
                Console.Write(stop - start); Console.CursorLeft = 43;
                Console.Write(countN); Console.CursorLeft = 53;
                Console.Write(count1); Console.CursorLeft = 63;
                Console.Write(count2);
                Console.WriteLine();
                //sc.AddString(sb.ToString());    //Debug
            }

            //sc.Compare();   //Debug
            //foreach (string s in sc.errors)
            //{
            //    Console.WriteLine(s + " - " + vg.Debug(Convert.ToInt32(s.Split(':')[0])));
            //}

            //for (int i = 0; i < 1000; i++)   //Debug
            //{
            //    Console.WriteLine(vg.Debug(ySize - 4000 + i));
            //}
        }
    }
}
