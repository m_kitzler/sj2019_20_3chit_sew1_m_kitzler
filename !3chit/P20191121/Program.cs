﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

namespace P20191121
{
    public static class MyExtensions
    {
        public static string ToHex(this int i)
        {
            return i.ToString("X");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(22.ToString("X"));
            Console.WriteLine(22.ToHex());  //etwas Verständlicher

            Console.WriteLine();
            Console.WriteLine((new Regex(@"\$.").Match("^A$B")).Value);

            Console.WriteLine();
            Regex rgx = new Regex(@"^[^0-9]+$");
            Console.WriteLine(rgx.IsMatch("aaaaaaa"));
            Console.WriteLine(rgx.IsMatch("aaaa1aa"));

            Console.WriteLine();
            rgx = new Regex(@"^[^A]+$");
            Console.WriteLine(rgx.IsMatch("aaaaaaa"));
            Console.WriteLine(rgx.IsMatch("aaaaAaa"));

            Console.WriteLine();
            rgx = new Regex(@"^[0-1]{5}$");
            Console.WriteLine(rgx.IsMatch("10010"));
            Console.WriteLine(rgx.IsMatch("010010"));
            Console.WriteLine(rgx.IsMatch("01201"));

            Console.WriteLine("\n\raaaaa1aaa:");
            Console.Write(new Regex(@"^\D+$").IsMatch("aaaaa1aaa")); Console.WriteLine("\t" + ((new Regex(@"^\D+$").Match("aaaaa1aaa")).Value.Length == 0 ? "NO VALUE" : (new Regex(@"^\D+$").Match("aaaaa1aaa")).Value));
            Console.Write(new Regex(@"^\D+").IsMatch("aaaaa1aaa")); Console.WriteLine("\t" + (new Regex(@"^\D+").Match("aaaaa1aaa")).Value);
            Console.Write(new Regex(@"\D+$").IsMatch("aaaaa1aaa")); Console.WriteLine("\t" + (new Regex(@"\D+$").Match("aaaaa1aaa")).Value);
            Console.Write(new Regex(@"\D+").IsMatch("aaaaa1aaa")); Console.WriteLine("\t" + (new Regex(@"\D+").Match("aaaaa1aaa")).Value);

            Console.WriteLine("\n\rdata: [USER]m.kitzler[MESSAGE]Hello World\\[FORMAT\\]Italic[FORMAT]Bold");
            string data = @"[USER]m.kitzler[MESSAGE]Hello World\[FORMAT\]Italic[FORMAT]Bold";
            Console.WriteLine((new Regex(@"(?<=\[USER\])[^[]*").Match(data)).Value);
            Console.WriteLine((new Regex(@"(?<=\[MESSAGE\]).*(.(?=\[(?<!\\)))").Match(data)).Value);
            //(?<=\[MESSAGE\]).* Der Inhalt, davor muss das Tag MESSAGE sein
            //(.(?=\[(?<!\\))) Am Schluss das Zeichen vor der eckigen Klammer, wobei vor der kein Backslash stehen darf
            Console.WriteLine((new Regex(@"(?<=\[FORMAT\])[^[]*").Match(data)).Value);

            Console.WriteLine();
            IFormatter f = new BinaryFormatter();
            using (Stream cs = Console.OpenStandardOutput())
            {
                f.Serialize(cs, 300);
            }
        }
    }
}
