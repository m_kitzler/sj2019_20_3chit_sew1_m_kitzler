﻿using System;
using System.Text;

namespace at.htlkrems
{
    public static class Extensions
    {
        public static string Reverse(this string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }
        public static bool IsPalindromWord(this string str)
        {
            if (str == null || str.Length == 0)
                return false;
            string hlp = str.ToLower();
            for (int i = 0; i <= Math.Floor((double)hlp.Length / 2); i++)
            {
                if (hlp[i] != hlp[hlp.Length - 1 - i])
                {
                    return false;
                }
            }
            return true;
        }
        public static bool IsPalindromSentence(this string str, params char[] c)
        {
            int variant = 3;
            if (variant == 1)
            {
                if (str == null || str.Length == 0)
                    return false;
                string hlp = str.ToLower();
                hlp = string.Join("", hlp.Split(c));
                bool isPalindrom = true;
                for (int i = 0; i <= Math.Floor((double)hlp.Length / 2); i++)
                {
                    if (hlp[i] != hlp[hlp.Length - 1 - i])
                    {
                        isPalindrom = false;
                    }
                }
                return isPalindrom;
            }
            if (variant == 2)
            {
                if (str == null || str.Length == 0)
                    return false;
                string hlp = str.ToLower();
                for (int iLeft = 0, iRight = hlp.Length - 1; iLeft < iRight; iLeft++, iRight--)
                {
                    while (Array.IndexOf(c, hlp[iLeft]) > -1 && iLeft != iRight)
                    {
                        iLeft++;
                    }
                    while (Array.IndexOf(c, hlp[iRight]) > -1 && iLeft != iRight)
                    {
                        iRight--;
                    }
                    if (hlp[iLeft] != hlp[iRight])
                    {
                        return false;
                    }
                }
                return true;
            }
            if (variant == 3)
            {
                if (str == null || str.Length == 0)
                    return false;
                string hlp = str.ToLower();
                for (int iLeft = 0, iRight = hlp.Length - 1; iLeft < iRight; iLeft++, iRight--)
                {
                    bool L = false;
                    do
                    {
                        iLeft++;
                    } while (L && iLeft != iRight);
                    while (IsInArray(c, hlp[iRight]) && iLeft != iRight)
                    {
                        iRight--;
                    }
                    if (hlp[iLeft] != hlp[iRight])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}
