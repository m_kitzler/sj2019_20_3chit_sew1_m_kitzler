﻿using System;
using System.IO;
using System.Text;

namespace at.htlkrems._3chit.games
{
    [Serializable]
    public class VierGewinnt : ISpielbar
    {
        int xSize, ySize;
        string[,] field;
        int[] nextFree;

        //Zum Feststellen wer dran ist
        string lastSet = "";

        /// <summary>
        /// Gibt die Größe des Feldes vor.
        /// </summary>
        /// <param name="x">Die horizontale Größe</param>
        /// <param name="y">Die vertikale Größe</param>
        public VierGewinnt(int x, int y)
        {
            xSize = x;
            ySize = y;
            field = new string[x, y];
            nextFree = new int[xSize];
            for (int i = 0; i < xSize; i++)
            {
                nextFree[i] = ySize - 1;
            }
        }

        public VierGewinnt() { }

        /// <summary>
        /// Legt einen Stein in die angegebene Spalte.
        /// </summary>
        /// <param name="column"></param>
        public Tuple<int> Setze(int column, string wert)
        {
            //if (column >= xSize)
            //    return Tuple.Create(-1);
            //for (int i = ySize - 1; i >= 0; i--)
            //{
            //    if (field[column, i] == null)
            //    {
            //        field[column, i] = wert;
            //        return Tuple.Create(i);
            //    }
            //}
            if (column < 0 || column >= xSize)      //Faster
                return Tuple.Create(-1);
            if (nextFree[column] >= 0)
            {
                field[column, nextFree[column]] = wert;
                nextFree[column]--;

                lastSet = wert;

                return Tuple.Create(nextFree[column] + 1);
            }
            return Tuple.Create(-1);
        }

        /// <summary>
        /// Legt einen Stein in die angegebene Spalte und ermittelt gewinner
        /// </summary>
        /// <param name="column"></param>
        /// <param name="wert"></param>
        /// <returns></returns>
        public Tuple<int, string> SetzeGE(int column, string wert)
        {
            if (column < 0 || column >= xSize)
                return new Tuple<int, string>(-1, null);
            for (int y = ySize - 1; y >= 0; y--)
            {
                if (field[column, y] == null)
                {
                    field[column, y] = wert;
                    lastSet = wert;
                    #region Gewinnermittliung
                    //Senkrecht
                    if (y + 3 < ySize && field[column, y] == field[column, y + 1] && field[column, y] == field[column, y + 2] && field[column, y] == field[column, y + 3])
                        return new Tuple<int, string>(y, field[column, y]);
                    //Waagrecht
                    int countLR = 0;
                    int countULDR = 0;  //Counter UpLeft DownRight = \
                    int countDLUR = 0;  //Counter DownLeft UpRight = /

                    for (int offset = 1; offset < 4; offset++)  //LeftRight
                    {
                        if (column - offset >= 0 && field[column - offset, y] == field[column, y])    //nach Links
                            countLR++;
                        else
                            break;
                    }
                    for (int offset = 1; offset < 4; offset++)
                    {
                        if (column + offset < xSize && field[column + offset, y] == field[column, y])    //Rechts
                            countLR++;
                        else
                            break;
                    }
                    if (countLR >= 3)   //Bei 3 weiteren Treffern
                        return new Tuple<int, string>(y, field[column, y]);

                    for (int offset = 1; offset < 4; offset++)  //UpLeft DownRight
                    {
                        if (column - offset >= 0 && y - offset >= 0 && field[column - offset, y - offset] == field[column, y])    //UpLeft
                            countULDR++;
                        else
                            break;
                    }
                    for (int offset = 1; offset < 4; offset++)
                    {
                        if (column + offset < xSize && y + offset < ySize && field[column + offset, y + offset] == field[column, y])    //DownRight
                            countULDR++;
                        else
                            break;
                    }
                    if (countULDR >= 3)
                        return new Tuple<int, string>(y, field[column, y]);

                    for (int offset = 1; offset < 4; offset++)  //DownLeft UpRight
                    {
                        if (column - offset >= 0 && y + offset < ySize && field[column - offset, y + offset] == field[column, y])    //DownLeft
                            countDLUR++;
                        else
                            break;
                    }
                    for (int offset = 1; offset < 4; offset++)
                    {
                        if (column + offset < xSize && y - offset >= 0 && field[column + offset, y - offset] == field[column, y])    //UpRight
                            countDLUR++;
                        else
                            break;
                    }
                    if (countDLUR >= 3)
                        return new Tuple<int, string>(y, field[column, y]);
                    #endregion
                    return new Tuple<int, string>(y, null);
                }
            }
            return new Tuple<int, string>(-1, null);
        }

        public string Gewinnermittlung()
        {
            string ret = GEVertikal();
            if (ret != null)
            {
                Reset();
                return ret;
            }
            ret = GEHorizontal();
            if (ret != null)
            {
                Reset();
                return ret;
            }
            ret = GEDiagonal();
            if (ret != null)
            {
                Reset();
                return ret;
            }
            if (FieldFull())
            {
                Reset();
                return "draw";
            }
            return null;
        }

        private bool FieldFull()
        {
            for (int y = 0; y < ySize; y++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    if (field[x, y] == null)
                        return false;
                }
            }
            return true;
        }

        private string GEVertikal()
        {
            for (int x = 0; x < xSize; x++)
            {
                string lastP = null;    //wird gespeichert, wer gerade den count hat
                int count = 0;
                for (int y = 0; y < ySize; y += 2)  //jedes zweite wird überprüft
                {
                    if (field[x, y] != null)
                    {
                        if (lastP == null)  //erstmaliges festlegen
                        {
                            lastP = field[x, y];
                            count = 1;
                        }
                        else if (lastP == field[x, y])  //increase
                        {
                            count++;
                        }
                        else if (lastP != field[x, y])  //wechsel und reset
                        {
                            lastP = field[x, y];
                            count = 1;
                        }
                        if (count >= 2)     //Veranschaulichung: [?][Spieler][?][Spieler][?]
                        {
                            if (field[x, y - 1] == lastP)   //check ob in der Mitte auch lastP
                            {
                                if (y + 1 < xSize && field[x, y + 1] == lastP)  //check ob ganz recht lastP
                                    return lastP;
                                if (y - 3 >= 0 && field[x, y - 3] == lastP)     //check ob ganz links lastP
                                    return lastP;
                            }
                        }
                    }
                    else    //Reset bei null
                        count = 0;
                }
            }
            return null;
        }

        public string GEHorizontal()
        {
            for (int y = 0; y < ySize; y++)
            {
                string lastP = null;    //wird gespeichert, wer gerade den count hat
                int count = 0;
                for (int x = 0; x < xSize; x += 2)  //jedes zweite wird überprüft
                {
                    if (field[x, y] != null)
                    {
                        if (lastP == null)  //erstmaliges festlegen
                        {
                            lastP = field[x, y];
                            count = 1;
                        }
                        else if (lastP == field[x, y])  //increase
                        {
                            count++;
                        }
                        else if (lastP != field[x, y])  //wechsel und reset
                        {
                            lastP = field[x, y];
                            count = 1;
                        }
                        if (count >= 2)     //Veranschaulichung: [?][Spieler][?][Spieler][?]
                        {
                            if (field[x - 1, y] == lastP)   //check ob in der Mitte auch lastP
                            {
                                if (x + 1 < xSize && field[x + 1, y] == lastP)  //check ob ganz recht lastP
                                    return lastP;
                                if (x - 3 >= 0 && field[x - 3, y] == lastP)     //check ob ganz links lastP
                                    return lastP;
                            }
                        }
                    }
                    else    //Reset bei null
                        count = 0;
                }
            }
            return null;
        }

        public string GEHorizontal(int y)
        {
            for (int x = 0; x < xSize - 3; x++)
            {
                if (field[x, y] != null && field[x, y] == field[x + 1, y] && field[x, y] == field[x + 2, y] && field[x, y] == field[x + 3, y])
                    return field[x, y];
            }
            return null;
        }

        public string GEHLug(int y)
        {
            string pi1 = "SP1", pi2 = "SP2";
            int p1 = 0, p2 = 0;
            for (int x = 0; x < xSize; x++)
            {
                if (field[x, y] != null)
                {
                    if (field[x, y] == pi1)
                    {
                        p1++;
                        p2 = 0;
                    }
                    if (field[x, y] == pi2)
                    {
                        p2++;
                        p1 = 0;
                    }

                    if (p1 >= 4)
                    {
                        return pi1;
                    }
                    if (p2 >= 4)
                    {
                        return pi2;
                    }
                }
                else
                {
                    p1 = 0;
                    p2 = 0;
                }
            }
            return null;
        }

        public string GEHPoem(int y)
        {
            string lastP = null;
            int count = 0;
            for (int x = 0; x < xSize; x++)
            {
                if (field[x, y] != null)
                {
                    if (lastP == null)
                    {
                        lastP = field[x, y];
                        count = 1;  //ä
                    }
                    else if (lastP == field[x, y])
                    {
                        count++;
                    }
                    else if (lastP != field[x, y])
                    {
                        lastP = field[x, y];
                        count = 1;  //ä
                    }
                    if (count >= 4)
                    {
                        return lastP;
                    }
                }
                else            //ä
                    count = 0;  //ä
            }
            return null;
        }

        public string GEHPoemMod(int y)
        {
            string lastP = null;
            int count = 0;
            for (int x = 0; x < xSize; x += 2)
            {
                if (field[x, y] != null)
                {
                    if (lastP == null)
                    {
                        lastP = field[x, y];
                        count = 1;    //ä
                    }
                    else if (lastP == field[x, y])
                    {
                        count++;
                    }
                    else if (lastP != field[x, y])
                    {
                        lastP = field[x, y];
                        count = 1;  //ä
                    }
                    if (count >= 2)
                    {
                        if (field[x - 1, y] == lastP)
                        {
                            if (x + 1 < xSize && field[x + 1, y] == lastP)
                                return lastP;
                            if (x - 3 >= 0 && field[x - 3, y] == lastP)
                                return lastP;
                        }
                    }
                }
                else            //ä
                    count = 0;  //ä
            }
            return null;
        }

        public string Debug(int row)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < xSize; i++)
            {
                if (field[i, row] == null)
                    sb.Append("null ");
                else
                    sb.Append(field[i, row] + "  ");
            }
            return sb.ToString();
        }

        private string GEDiagonal()
        {
            for (int x = 0; x < xSize - 3; x++)    //Diagonal von links oben nach rechts unten
            {
                for (int y = 0; y < ySize - 3; y++)
                {
                    if (field[x, y] == field[x + 1, y + 1] && field[x, y] == field[x + 2, y + 2] && field[x, y] == field[x + 3, y + 3] && field[x, y] != null)
                        return field[x, y];
                }
            }
            for (int x = 3; x < xSize; x++)    //Diagonal von rechts oben nach links unten
            {
                for (int y = 0; y < ySize - 3; y++)
                {
                    if (field[x, y] == field[x - 1, y + 1] && field[x, y] == field[x - 2, y + 2] && field[x, y] == field[x - 3, y + 3] && field[x, y] != null)
                        return field[x, y];
                }
            }
            return null;
        }

        public void Reset()
        {
            field = new string[xSize, ySize];
            for (int i = 0; i < xSize; i++)
            {
                nextFree[i] = ySize - 1;
            }
        }

        public void SaveTo(string path)
        {
            using (StreamWriter sw = new StreamWriter(new FileStream(path, FileMode.Create)))
            {
                for (int y = ySize - 1; y >= 0; y--)
                {
                    for (int x = 0; x < xSize; x++)
                    {
                        sw.WriteLine(field[x, y]);
                    }
                }
                sw.WriteLine("LASTSET");
                sw.WriteLine(lastSet);
                sw.Write("END");
            }
        }

        public Stream LoadFrom(string path)
        {
            return new FileStream(path, FileMode.Open);
        }
    }
}
