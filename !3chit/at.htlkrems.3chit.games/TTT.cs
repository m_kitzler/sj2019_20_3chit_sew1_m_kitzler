﻿using System;

namespace at.htlkrems._3chit.games
{
    public class TTT : ISpielbar
    {
        string[,] field = new string[3, 3];

        public Tuple<int> Setze(int nr, string wert)
        {
            field[(nr - 1) % 3, (int)Math.Floor(((double)(nr - 1) / 3))] = wert;
            return Tuple.Create(0);
        }

        /// <summary>
        /// Gibt den Gewinner zurück, draw bei gleichstand oder null wenn keines von Beiden
        /// </summary>
        /// <returns></returns>
        public string Gewinnermittlung()
        {
            for (int x = 0; x < 3; x++) //vertikal
            {
                if (field[x, 0] != null && field[x, 0] == field[x, 1] && field[x, 0] == field[x, 2])
                {
                    string winner = field[x, 0];
                    Reset();
                    return winner;
                }
            }
            for (int y = 0; y < 3; y++)    //horizontal
            {
                if (field[0, y] != null && field[0, y] == field[1, y] && field[0, y] == field[2, y])
                {
                    string winner = field[0, y];
                    Reset();
                    return winner;
                }
            }
            if (field[0, 0] != null && field[0, 0] == field[1, 1] && field[0, 0] == field[2, 2])
            {
                string winner = field[0, 0];
                Reset();
                return winner;
            }
            if (field[2, 0] != null && field[2, 0] == field[1, 1] && field[2, 0] == field[0, 2])
            {
                string winner = field[2, 0];
                Reset();
                return winner;
            }
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (field[x, y] == null)
                        return null;
                }
            }
            Reset();
            return "draw";
        }

        private void Reset()
        {
            field = new string[3, 3];
        }
    }
}
