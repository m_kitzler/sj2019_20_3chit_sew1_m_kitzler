﻿using System;

namespace at.htlkrems._3chit.games
{
    interface ISpielbar
    {
        Tuple<int> Setze(int nr, string wert);

        /// <summary>
        /// Gibt den Gewinner zurück, andernfalls null
        /// </summary>
        /// <returns></returns>
        string Gewinnermittlung();
    }
}
