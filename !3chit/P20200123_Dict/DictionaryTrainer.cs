﻿using System;
using System.Collections.Generic;
using System.IO;

namespace P20200123_Dict
{
    public class DictionaryTrainer
    {
        List<string[]> words = new List<string[]>();
        Dictionary<string, string> translations = new Dictionary<string, string>();
        int current = -1;
        public DictionaryTrainer() { }

        public DictionaryTrainer(string FilePath) => LoadFromFile(FilePath);

        public void LoadFromFile(string Path)
        {
            using (StreamReader sr = new StreamReader(new FileStream(Path, FileMode.Open)))
            {
                string temp = sr.ReadLine();
                while (temp != null)
                {
                    string[] hlp = temp.Split('=');
                    words.Add(new string[] { hlp[0], "0", "0" });   //Wort, Anzahl richtig, Anzahl falsch
                    translations.Add(hlp[0], hlp[1]);
                    temp = sr.ReadLine();
                }
            }
            GetWordAndTranslation(0);
        }

        /// <summary>
        /// Wählt ein neues Wort und gibt es und seine Übersetzung in Form von string[] zurück.
        /// </summary>
        /// <param name="Reverse">Dreht Wort und Übersetzung um, wenn größer als Null.</param>
        /// <returns></returns>
        public string[] GetWordAndTranslation(int Reverse = 0)
        {
            if (words.Count > 0)
            {
                int r;
                double chanceMultiplier;
                string[] word;
                do
                {
                    current++;
                    if (current >= words.Count)
                        current = 0;
                    word = words[current];
                    r = new Random().Next(0, 101);
                    chanceMultiplier = (1 - Convert.ToInt32(words[current][1]) / ((Convert.ToInt32(words[current][2]) == 0) ? 1 : Convert.ToInt32(words[current][2])));
                } while (r > ((chanceMultiplier < 0) ? 50 + chanceMultiplier * 100 : 50));  //Basischance 50, bei Wahr/Falsch-Quote von unter 1 -> plus (1 - Quote) * 100
                if (Reverse <= 0)
                    return new string[] { word[0], translations[word[0]] };
                else
                    return new string[] { translations[word[0]], word[0] };
            }
            else
                throw new Exception("Es wurden keine Vokabeln aus einer Datei geladen.");
        }

        /// <summary>
        /// Teilt dem Programm mit, ob das Wort falsch oder richtig war.
        /// </summary>
        /// <param name="b"></param>
        public void WordCorrect(bool b)
        {
            if (current < 0)
            {
                throw new Exception("Es wurde noch kein Wort gegeben");
            }
            else if (b)
            {
                words[current][1] = (Convert.ToInt32(words[current][1]) + 1).ToString();
            }
            else
            {
                words[current][2] = (Convert.ToInt32(words[current][2]) + 1).ToString();
            }
        }

        /// <summary>
        /// Prüft, ob die Eingabe des Benutzers richtig war und gibt die Fehlerpositionen zurück.
        /// Werden keine Fehler gefunden wird "null" zurückgegeben.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public int[] Check(string input)
        {
            List<int> retList = new List<int>();
            if (input.Length != words[current][0].Length)
            {
                //TODO: Prüfen, wieviel zu i hinzu
            }
            return (retList.Count > 0) ? retList.ToArray() : null;
        }
    }
}
