﻿using System;
using System.Collections.Generic;

namespace P20200213._1_Events
{
    public class JokeEventArgs : EventArgs
    {
        public string quality { get; set; }
        public JokeEventArgs(string quality)
        {
            this.quality = quality;
        }
    }

    public class AgingEventArgs : EventArgs
    {
        public AgingEventArgs(string message)
        {
            this.message = message;
        }
        public string message { get; set; }
    }

    class Person
    {
        Random r;
        string lachen, booing;
        int alter;
        //public delegate void Tellme(string text);
        //public event Tellme Erwachsen;
        public delegate void AgingEventHandler(object sender, AgingEventArgs e); //Übliche Schreibweise
        public event AgingEventHandler Erwachsen;   //Man kann auch den vordefinierten delegate "EventHandler" verwenden

        public Person(string lachen, string booing, Random r)
        {
            this.lachen = lachen;
            this.booing = booing;
            this.r = r;
        }

        public void Altern()
        {
            alter++;
            if (Erwachsen != null && alter >= 18)
                Erwachsen(this, new AgingEventArgs("bin Erwachsen"));
            //Erwachsen("bin Erwachsen");
        }

        public void Reagieren(object s, EventArgs e)
        {
            if ((e as JokeEventArgs).quality == "GOOD")
            {
                if (r.Next(0, 3) == 0)
                    Console.WriteLine(lachen);
            }
            else if ((e as JokeEventArgs).quality == "BAD")
            {
                if (r.Next(0, 3) == 0)
                    Console.WriteLine(booing);
            }
            else if ((e as JokeEventArgs).quality == "VERY BAD")
            {
                Console.WriteLine(booing);
            }
        }
    }

    class Joker
    {
        public event EventHandler WitzErzählt;
        public List<string[]> witze { get; set; } = new List<string[]>();

        private Random r;

        public Joker(Random r)
        {
            this.r = r;
        }

        public void AddJoke(string joke, string quality) => witze.Add(new string[] { joke, quality });

        public void Erzähle()
        {
            int Witz = r.Next(0, witze.Count);
            Console.WriteLine(witze[Witz][0]);
            WitzErzählt?.Invoke(this, new JokeEventArgs(witze[Witz][1]));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Person p = new Person();
            //  //p.Erwachsen += (text) => Console.WriteLine(text);
            //p.Erwachsen += (sender, e) => Console.WriteLine(e.message + " - " + sender.ToString());
            //for (int i = 0; i < 18; i++)
            //    p.Altern();

            Random r = new Random();
            Joker j = new Joker(r);
            j.AddJoke("Lustiger Witz", "GOOD");
            j.AddJoke("Schlechter Witz", "BAD");
            j.AddJoke("Der kürzeste Jägerwitz: Treffer!", "GOOD");
            j.AddJoke("Ein Jäger geht am Wirtshaus vorbei", "GOOD");
            j.AddJoke("Was sagt der große Stift zum kleinen Stift? Wachsmalstift.", "VERY BAD");
            Person p = new Person("Hihihi", "Boo!", r);
            Person p1 = new Person("Hahaha", "FAAAAD!", r);
            Person p2 = new Person("Hohoho", "BOOOOO!", r);
            Person p3 = new Person("Hahahihi", "Schrecklich!", r);
            Person p4 = new Person("HAHAHAHA", "Minus!", r);
            j.WitzErzählt += p.Reagieren;
            j.WitzErzählt += p1.Reagieren;
            j.WitzErzählt += p2.Reagieren;
            j.WitzErzählt += p3.Reagieren;
            j.WitzErzählt += p4.Reagieren;
            do
            {
                Console.Clear();
                j.Erzähle();
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Q);
        }
    }
}
