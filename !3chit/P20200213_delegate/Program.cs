﻿using System;

namespace P20200213_delegate
{
    class Program
    {
        delegate int Rechner(int a, int b);
        static void Main(string[] args)
        {
            Rechner add = Addiere;
            Console.WriteLine(add(3, 5));

            Rechner mult = (a, b) => a * b;
            Console.WriteLine(mult(5, 6));
        }

        static int Addiere(int a, int b)
        {
            return a + b;
        }
    }
}
