﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace P20191010
{
    public partial class Form1 : Form
    {
        AnimalShelter animalShelter;
        public Form1()
        {
            InitializeComponent();
        }

        private void LadenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((animalShelter == null || animalShelter.Count < 1) || MessageBox.Show("Wenn sie eine neue Tabelle laden wird die Alte gelöscht.\r\nWollen sie fortfahren?", "Warnung", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                openFileDialog.Filter = "Tierpension Tabelle (*.shelter)|*.shelter|Tabelle (*.csv)|*.csv|Alle Dateiarten (*.*)|*.*";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    switch (Path.GetExtension(openFileDialog.FileName))
                    {
                        case ".csv":
                            animalShelter = new AnimalShelter(File.ReadAllLines(openFileDialog.FileName));
                            break;
                        case ".shelter":
                            using (Stream s = new FileStream(openFileDialog.FileName, FileMode.Open))
                            {
                                IFormatter f = new BinaryFormatter();
                                animalShelter = (AnimalShelter)f.Deserialize(s);
                            }
                            break;
                    }
                    dataGridView1.Rows.Clear();
                    for (int i = 0; i < animalShelter.Count; i++)
                    {
                        dataGridView1.Rows.Add(animalShelter[i].ToString().Split(';'));
                    }
                }
            }
        }

        private void SpeichernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Tierpension Tabelle (*.shelter)|*.shelter|Tabelle (*.csv)|*.csv|Alle Dateiarten (*.*)|*.*";
            if (animalShelter != null && saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                switch (Path.GetExtension(saveFileDialog.FileName))
                {
                    case ".csv":
                        File.WriteAllLines(saveFileDialog.FileName, animalShelter.ToCSV());
                        break;
                    case ".shelter":
                        using (Stream s = new FileStream(saveFileDialog.FileName, FileMode.Create))
                        {
                            IFormatter f = new BinaryFormatter();
                            f.Serialize(s, animalShelter);
                        }
                        break;
                }
            }
        }

        private void HinzufügenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddAnimalDialog a = new AddAnimalDialog();
            if (a.ShowDialog() == DialogResult.OK)
            {
                if (animalShelter == null)
                {
                    animalShelter = new AnimalShelter();
                }
                animalShelter.AddFromCSV(a.NewAnimal);
                dataGridView1.Rows.Add(animalShelter[animalShelter.Count - 1].ToString().Split(';'));
            }
        }
    }
}
