﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace P20191010
{
    [Serializable]
    class AnimalShelter : ISerializable
    {
        private List<Animal> animals;
        public int Count { get; private set; }

        public AnimalShelter(SerializationInfo info, StreamingContext con)
        {
            animals = (List<Animal>)info.GetValue("Animals", typeof(List<Animal>));
            Count = (int)info.GetValue("Count", typeof(int));
        }

        public AnimalShelter()
        {
            this.animals = new List<Animal>();
            Count = 0;
        }

        public AnimalShelter(Animal[] animals)
        {
            this.animals = new List<Animal>(animals);
            Count = this.animals.Count;
        }

        public AnimalShelter(string[] saveFile)
        {
            animals = new List<Animal>();
            for (int i = 1; i < saveFile.Length; i++)
            {
                AddFromCSV(saveFile[i]);
            }
            Count = animals.Count;
        }

        public Animal this[int i]
        {
            get
            {
                return animals[i];
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext con)
        {
            info.AddValue("Animals", animals, typeof(List<Animal>));
            info.AddValue("Count", Count, typeof(int));
        }

        public void Add(Animal toAdd)
        {
            animals.Add(toAdd);
            Count = animals.Count;
        }

        public void AddFromCSV(string toAdd)
        {
            string[] hlp = toAdd.Split(';');
            DateTime dateOfBirth = new DateTime(Convert.ToInt32(hlp[2].Split('.')[2]), Convert.ToInt32(hlp[2].Split('.')[1]), Convert.ToInt32(hlp[2].Split('.')[0]));
            DateTime dateOfAdmission = new DateTime(Convert.ToInt32(hlp[3].Split('.')[2]), Convert.ToInt32(hlp[3].Split('.')[1]), Convert.ToInt32(hlp[3].Split('.')[0]));
            DateTime dateOfVacc = new DateTime(Convert.ToInt32(hlp[4].Split('.')[2]), Convert.ToInt32(hlp[4].Split('.')[1]), Convert.ToInt32(hlp[4].Split('.')[0]));
            switch (hlp[0])
            {
                case "Dog":
                    animals.Add(new Dog(hlp[1], dateOfBirth, dateOfAdmission, dateOfVacc, hlp[5]));
                    break;
                case "Cat":
                    animals.Add(new Cat(hlp[1], dateOfBirth, dateOfAdmission, dateOfVacc, hlp[5]));
                    break;
                case "Bird":
                    animals.Add(new Bird(hlp[1], dateOfBirth, dateOfAdmission, dateOfVacc, hlp[5]));
                    break;
            }
            Count = animals.Count;
        }

        public string[] ToCSV()
        {
            List<string> hlp = new List<string>();
            hlp.Add("Typ;Name;Geburtsdatum;Aufnahmedatum;Impfungsdatum;Besonderheiten");
            foreach (Animal a in animals)
            {
                hlp.Add(a.ToString());
            }
            return hlp.ToArray();
        }
    }
}
