﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace P20191010
{
    public partial class AddAnimalDialog : Form
    {
        public string NewAnimal { get; private set; }
        private List<TextBox> specials = new List<TextBox>();
        private Dictionary<TextBox, Button> findSpecialBtn = new Dictionary<TextBox, Button>();
        public AddAnimalDialog()
        {
            InitializeComponent();
            specials.Add(textBoxSpecial);
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            if (comboBoxType.SelectedIndex >= 0 && textBoxName.Text != "" && dateTimePickerDateOfAdmission.Format == DateTimePickerFormat.Long)
            {
                StringBuilder NewAnimal = new StringBuilder(comboBoxType.Text + ";");
                NewAnimal.Append(textBoxName.Text + ";");
                if (dateTimePickerDateOfBirth.Format == DateTimePickerFormat.Custom)
                {
                    NewAnimal.Append(dateTimePickerDateOfBirth.Value.ToString("dd/MM/yyyy"));
                }
                NewAnimal.Append(";");
                NewAnimal.Append(dateTimePickerDateOfAdmission.Value.ToString("dd/MM/yyyy") + ";");
                if (dateTimePickerDateOfVacc.Format == DateTimePickerFormat.Custom)
                {
                    NewAnimal.Append(dateTimePickerDateOfVacc.Value.ToString("dd/MM/yyyy"));
                }
                NewAnimal.Append(";");
                for (int i = 0; i < specials.Count - 1; i++)
                {
                    NewAnimal.Append(specials[i].Text + ",");
                }
                NewAnimal.Append(specials[specials.Count - 1]);
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MessageBox.Show("Typ, Name und Aufnahmedatum müssen einen Wert haben!");
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void DateTimePickerDateOf_ValueChanged(object sender, EventArgs e)
        {
            if (((DateTimePicker)sender).Checked)
            {
                ((DateTimePicker)sender).Format = DateTimePickerFormat.Long;
            }
            else
            {
                ((DateTimePicker)sender).Format = DateTimePickerFormat.Custom;
            }

        }

        private void ButtonAddSp_Click(object sender, EventArgs e)//TODO: Abstände komisch(vielleicht)
        {
            Point currPos = specials[specials.Count - 1].Location;
            Point nextPos = currPos;
            nextPos.Y += 29;
            TextBox newSpecialBox = new TextBox();
            newSpecialBox.Name = "textBoxSpecial";
            newSpecialBox.Size = new Size(171, 20);
            newSpecialBox.Location = nextPos;
            specials.Add(newSpecialBox);

            Button newSpecialButton = new Button();
            newSpecialButton.Name = "buttonRemoveSp";
            newSpecialButton.Size = new Size(23, 23);
            nextPos.X += 177;
            newSpecialButton.Location = nextPos;
            newSpecialButton.Text = "-";
            newSpecialButton.Click += ButtonRemoveSp_Click;
            newSpecialButton.Tag = newSpecialBox;

            findSpecialBtn.Add(newSpecialBox, newSpecialButton);

            panelData.Controls.Add(newSpecialBox);
            panelData.Controls.Add(newSpecialButton);
        }
        private void ButtonRemoveSp_Click(object sender, EventArgs e)
        {
            bool found = false;
            Button b = (sender as Button);
            for (int i = 1; i < specials.Count; i++)
            {
                if (found)
                {
                    Point box = specials[i].Location;
                    Point button = findSpecialBtn[specials[i]].Location;//TODO: AAAAAAAAAAA
                    box.Y -= 29;
                    button.Y -= 29;
                    specials[i].Location = box;
                    findSpecialBtn[specials[i]].Location = button;
                    //((TextBox)panelData.Controls.Find("textBoxSpecial", false)[i]) = specials[i];
                    //((Button)panelData.Controls.Find("buttonRemoveSp", false)[i]) = findSpecialBtn[specials[i]];  Kaputt
                }
                if ((TextBox)b.Tag == specials[i])
                {
                    panelData.Controls.Remove(specials[i]);
                    panelData.Controls.Remove(b);
                    specials.RemoveAt(i);
                    found = true;
                }
            }
        }
    }
}
