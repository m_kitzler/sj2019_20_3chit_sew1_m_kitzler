﻿using at.htlkrems._3chit.games;
using Microsoft.Win32;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace P20200430_4Gewinnt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VierGewinnt vg = new VierGewinnt(7, 6);
        int player = 1;
        string p1 = "SP1";
        string p2 = "SP2";
        int xSize = 7;
        int ySize = 6;

        public MainWindow()
        {
            InitializeComponent();
            InitializeField();
            DisplayCurrentPlayer();
        }

        private void DisplayCurrentPlayer()
        {
            Resources["currPlayer"] = (player == 1 ? p1 : p2) + " ist dran";
        }

        private void InitializeField()
        {
            for (int y = 0; y < ySize; y++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    if ((y * xSize + x) == 0)   //Erstes schon erstellt
                        continue;
                    TextBlock tb = new TextBlock();
                    tb.HorizontalAlignment = HorizontalAlignment.Stretch;
                    tb.VerticalAlignment = VerticalAlignment.Stretch;
                    tb.TextAlignment = TextAlignment.Center;
                    tb.SetResourceReference(TextBlock.TextProperty, "field" + x + "-" + y);
                    tb.SetResourceReference(Control.FontSizeProperty, "fontSize");
                    Border b = new Border();
                    b.BorderThickness = new Thickness(1);
                    b.BorderBrush = new SolidColorBrush(Colors.Black);
                    b.Child = tb;
                    Grid.SetRow(b, y + 1);
                    Grid.SetColumn(b, x);
                    mainGrid.Children.Add(b);
                }
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Resources["fontSize"] = tb0.ActualHeight / 1.4;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int col = Convert.ToInt32((sender as Control).Tag);
            Setzen(col);
        }

        private void Setzen(int col, string wert = null)
        {
            Tuple<int, string> ret = vg.SetzeGE(col, wert != null ? wert : (player == 1 ? p1 : p2));
            if (ret.Item1 >= 0)
            {
                Resources["field" + col + "-" + ret.Item1] = (wert != null ? wert : (player == 1 ? p1 : p2));
            }
            else
            {
                MessageBox.Show("In dieser Spalte ist kein Platz");
            }
            if (ret.Item2 != null)
            {
                if (ret.Item2 == "draw")
                {
                    MessageBox.Show("Unentschieden");
                }
                else
                {
                    MessageBox.Show("Spieler " + ret.Item2 + " hat gewonnen");
                }
                ResetField();
            }
            player = (player == 1) ? 2 : 1;
            DisplayCurrentPlayer();
        }

        private void ResetField()
        {
            for (int x = 0; x < xSize; x++)
            {
                for (int y = 0; y < ySize; y++)
                {
                    Resources["field" + x + "-" + y] = null;
                }
            }
            vg.Reset();
        }

        private void button_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Control).Opacity = 0.7;
        }

        private void button_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Control).Opacity = 0;
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Vier Gewinnt Datei (*.vg)|*.vg";
            ofd.CheckFileExists = true;
            if (ofd.ShowDialog() == true)
            {
                using (StreamReader sr = new StreamReader(vg.LoadFrom(ofd.FileName)))
                {
                    int count = 0;
                    string data = sr.ReadLine();    //Laden geht irgendwie nicht
                    while (data != "LASTSET")
                    {
                        if (!String.IsNullOrEmpty(data))
                        {
                            Setzen(count / ySize, data);
                        }
                        data = sr.ReadLine();
                        count++;
                    }
                    data = sr.ReadLine();
                    if (data == "SP1")
                        player = 2;
                    else
                        player = 1;
                    Resources["currPlayer"] = (player == 1 ? p1 : p2) + " ist dran";
                }
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Vier Gewinnt Datei (*.vg)|*.vg";
            if (sfd.ShowDialog() == true)
            {
                vg.SaveTo(sfd.FileName);
            }
        }
    }
}
