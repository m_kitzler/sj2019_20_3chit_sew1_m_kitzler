﻿using P20191107_Lib;
using System;

namespace P20191107
{
    class Program
    {
        static void Main(string[] args)
        {
            DynList<int> dl = new DynList<int>(new Element<int>(55));
            dl.AddAtEnd(new Element<int>(56));
            dl.AddAtEnd(new Element<int>(57));
            dl.AddAtBegin(new Element<int>(54));
            dl.Print();
            Console.WriteLine(dl.Anzahl());

        }
    }
}
