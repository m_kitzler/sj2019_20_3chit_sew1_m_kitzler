﻿using System.Data.OleDb;
using System.Windows.Forms;

namespace P20200416_Access
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            string connect = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\\Users\\propl\\Documents\\Database1.accdb; Persist Security Info = False;";
            string query = "select * from Klasse";
            using (OleDbConnection conn = new OleDbConnection(connect))
            {
                OleDbCommand command = new OleDbCommand(query, conn);
                conn.Open();
                using (OleDbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        //if (reader.GetValue(1) != )
                        MessageBox.Show(reader.GetValue(0).ToString() + ": " + reader.GetValue(1).ToString() + " " + reader.GetValue(2).ToString());
                    }
                }
            }
        }
    }
}
