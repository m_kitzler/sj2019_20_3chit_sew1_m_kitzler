﻿using System;
using System.Data.OleDb;

namespace Konsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string connect = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\\Users\\propl\\Documents\\Database1.accdb; Persist Security Info = False;";
            string query = "select * from Klasse";
            using (OleDbConnection conn = new OleDbConnection(connect))
            {
                OleDbCommand command = new OleDbCommand(query, conn);
                conn.Open();
                using (OleDbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        //if (reader.GetValue(1) != )
                        Console.WriteLine(reader.GetValue(0).ToString() + ": " + reader.GetValue(1).ToString() + " " + reader.GetValue(2).ToString());
                    }
                }
            }
        }
    }
}
