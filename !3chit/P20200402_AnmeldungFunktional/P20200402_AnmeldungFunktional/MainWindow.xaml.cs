﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace P20200402_AnmeldungFunktional
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TextBox[] textBoxes;
        ComboBox[] comboBoxesOther;
        ComboBox[] comboBoxesGrades;
        public MainWindow()
        {
            InitializeComponent();
            textBoxes = new TextBox[] { textBoxParent1, textBoxParent2, textBoxName, textBoxAddress, textBoxTel, textBoxSchoolAddress };
            comboBoxesGrades = new ComboBox[] { comboBoxAM, comboBoxD, comboBoxE, comboBoxNW };
            comboBoxesOther = new ComboBox[] { comboBoxEW, comboBoxA1, comboBoxA2 };
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.OverwritePrompt = false;
            fd.Filter = "CSV Tabelle (*.csv)|*.csv";
            fd.DefaultExt = ".csv";
            int hlp = CheckFields();
            if (hlp == 200)
            {
                if (fd.ShowDialog() == true)
                {
                    StringBuilder sb = new StringBuilder((File.Exists(fd.FileName)) ? "\n" : "Elternteil 1;Elternteil2;Schuelername;Geburtstag;Adresse;Telefon;Schuladresse;Mathematik;Deutsch;Englisch;Naturwissenschaften;Erstwunsch;Alternative 1;Alternative2\n");
                    for (int i = 0; i < textBoxes.Length; i++)
                    {
                        sb.Append(textBoxes[i].Text + ";");
                        if (i == 2)
                        {
                            sb.Append(datePickerBirth.SelectedDate.Value.Date.ToShortDateString() + ";");
                        }
                    }
                    foreach (ComboBox cb in comboBoxesGrades)
                    {
                        sb.Append((cb.SelectedItem as ComboBoxItem).Content + ";");
                    }
                    foreach (ComboBox cb in comboBoxesOther)
                    {
                        sb.Append((cb.SelectedItem as ComboBoxItem).Content + ";");
                    }
                    File.AppendAllText(fd.FileName, sb.ToString());
                    MessageBox.Show("Vielen dank für ihre Anmeldung!");
                    Close();
                }
            }
            else if (hlp == 403)
            {
                MessageBox.Show("Der Notendurchschnitt des Schülers ist zu schlecht");
            }
            else if (hlp == 404)
            {
                MessageBox.Show("Ein oder mehrere Felder sind nicht ausgefüllt");
            }
        }

        private int CheckFields()
        {
            int retVal = 200;
            foreach (TextBox tb in textBoxes)
            {
                if (tb.Name != "textBoxParent2")
                {
                    if (tb.Text == "")
                    {
                        retVal = 404;
                        tb.BorderBrush = Brushes.Red;
                        tb.BorderThickness = new Thickness(2);
                    }
                    else
                    {
                        tb.BorderBrush = default;
                        tb.BorderThickness = default;
                    }
                }
            }
            if (datePickerBirth.SelectedDate == null)
            {
                retVal = 404;
                datePickerBirth.BorderBrush = Brushes.Red;
                datePickerBirth.BorderThickness = new Thickness(2);
            }
            else
            {
                datePickerBirth.BorderBrush = default;
                datePickerBirth.BorderThickness = default;
            }

            foreach (ComboBox cb in comboBoxesOther)
            {
                if (cb.SelectedIndex == 0)
                {
                    retVal = 404;
                    //cb.BorderBrush = Brushes.Red;
                    cb.BorderThickness = new Thickness(2);
                }
                else
                {
                    cb.BorderBrush = default;
                    cb.BorderThickness = default;
                }
            }

            int grade = 0;
            foreach (ComboBox cb in comboBoxesGrades)
            {
                if (cb.SelectedIndex == 0)
                {
                    retVal = 404;
                    cb.BorderBrush = Brushes.Red;
                    cb.BorderThickness = new Thickness(2);
                }
                else
                {
                    int hlp = Convert.ToInt32((cb.SelectedItem as ComboBoxItem).Content);
                    if (hlp > 4)
                    {
                        retVal = 403;
                    }
                    else
                    {
                        grade += hlp;
                    }
                    cb.BorderBrush = default;
                    cb.BorderThickness = default;
                }
            }
            if (grade / 4.0 > 4)
            {
                retVal = 403;
            }
            return retVal;
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxesOther != null)
            {
                foreach (ComboBox cb in comboBoxesOther)
                {
                    for (int i = 0; i < cb.Items.Count; i++)
                    {
                        if (i == comboBoxEW.SelectedIndex || i == comboBoxA1.SelectedIndex || i == comboBoxA2.SelectedIndex)
                            (cb.Items[i] as ComboBoxItem).IsEnabled = false;
                        else
                            (cb.Items[i] as ComboBoxItem).IsEnabled = true;
                    }
                }
            }
        }
    }
}
