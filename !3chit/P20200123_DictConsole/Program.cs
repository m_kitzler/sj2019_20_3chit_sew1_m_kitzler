﻿using P20200123_Dict;
using System;

namespace P20200123_DictConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            DictionaryTrainer dt = new DictionaryTrainer(Environment.ExpandEnvironmentVariables(@"%userprofile%\Desktop\Vokabel.txt"));
            while (!exit)
            {
                string[] word = dt.GetWordAndTranslation();
                Console.WriteLine(word[0]);
                Console.WriteLine($"{((Console.ReadLine().ToLower() == word[1].ToLower()) ? "Richtig" : "Falsch")}. Drücke eine beliebige Taste zum fortfahren.");
                Console.ReadKey(true);
                Console.Clear();
            }
        }
    }
}